﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// FROM:    https://docs.unity3d.com/ScriptReference/RequireComponent.html
//  Automatically adds the required component (if  not already existent); useful to avoid setup errors 
[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        //if ( Input.GetButton( "Fire1" ) )   // when mouse (Fire1) is held down
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            //  Normalize: Vector keeps same direction but length is 0.
            //  can be merged with above line?
            FireDirection.Normalize();
            //  Instantiates an instance of the prefab (?)
            //  Wondering if this is necessary, to create an instance
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;

            //  DESTROY SPHERE AFTER 3 SEC
            Destroy(prefabInstance, 3);

        }
    }
}
