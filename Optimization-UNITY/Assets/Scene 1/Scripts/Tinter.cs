﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        //  Gets the Renderer component
        //  Already using UnityEngine namespace; no need to call UnityEngine every time we use Random.Range?

        //  GetComponentInChildren vs Inspector assigning?
        //  https://forum.unity3d.com/threads/public-variable-vs-getcomponent.222395/
        //  Apparently there are no performance differences in the two
        GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );
    }

}
