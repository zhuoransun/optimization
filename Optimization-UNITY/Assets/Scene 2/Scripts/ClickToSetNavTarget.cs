﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        //  Gets the "Main Camera" game object
        //  Perhaps redundant?
        //  FROM:   http://answers.unity3d.com/questions/1170097/which-is-more-efficient-gameobjectfind-or-public-g.html
        //  GameObject.Find loops through all existing GameObjects and compares their names
        //  Recommended to use GameObject.FindWithTag
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();    //camera = GetComponent<Camera>() suffice??
        //  Returns a ray going from camera through a screen point
        //  FROM:   https://unity3d.com/learn/tutorials/topics/physics/physics-best-practices?playlist=30089
        //  Update() raycast usage may be overkill
        RaycastHit hit = new RaycastHit();  //get info back from raycast
        //  Moves NavMeshAgent to a destination determined by mouse pos

        //if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
        //{
        //    GetComponent<NavMeshAgent>().destination = hit.point;
        //}

        //  ONLY CHECK AND MOVE IF CLICKED
        if (Input.GetButtonDown("Fire1"))
        {
            Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground"));
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
    }
}
