﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        //  If there currently isn't an existing collectible, spawn one
        if ( m_currentCollectible == null )
        {
            //  UnityEngine is already in use
            //  Spawns a collectible at one of the spawn locations:
            //  random range from 0 to [number of children the collectibleSpawn has]
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.GetChildCount() ) ).position, Collectible.transform.rotation );
        }
    }
}
