﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    //  A script attached to the collictible game object.
    //  Destroys self if Player collides with it.
    //  OnCollisionEnter/etc is most likely a better way to do it (??)
    public void Update()
    {
        //Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        ////  a for loop??? for colliders
        ////  It's checking every collider until it finds one tagged Player
        
        //for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        //{
        //    if ( collidingColliders[colliderIndex].tag == "Player" )
        //    {
        //        Destroy( this.gameObject );
        //    }
        //}
    }

    //Instead of going through every collider known to man
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }

}
